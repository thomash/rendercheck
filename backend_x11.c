/**************************************************************************
 *
 * Copyright © 2004 Eric Anholt
 * Copyright © 2018 VMware, Inc., Palo Alto, CA., USA
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDERS, AUTHORS AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#include "rendercheck.h"
#include <assert.h>
#include <string.h>

struct rb_x11_format {
    struct rb_format rb_form;
    const XRenderPictFormat *form;
};

struct rb_x11 {
    struct rb rb;
    Display *dpy;
    struct _picture_info win;
    struct rb_x11_format *formats;
    int num_formats;
    int std_formats[STD_MAX];
    unsigned int last_error;
    bool catch_errors;
    int (*orig_error_handler)(Display *, XErrorEvent *);
    struct rb_x11 *next, *prev;
};

static struct rb_x11 *instances = NULL;

static const int std_format_map[] = {
    [STD_ARGB32] = PictStandardARGB32,
    [STD_RGB24] = PictStandardRGB24,
    [STD_A8] = PictStandardA8,
    [STD_A4] = PictStandardA4,
    [STD_A1] = PictStandardA1,
    [STD_MAX] = PictStandardNUM
};

static struct rb_x11 *
to_rb_x11(struct rb *rb)
{
    return (struct rb_x11 *) rb;
}

static int rb_x11_error_handler(Display *dpy, XErrorEvent *e)
{
    struct rb_x11 *instance = instances;

    for (instance = instances; instance != NULL; instance = instance->next) {
	if (instance->dpy == dpy) {
	    instance->last_error = e->error_code;
	    break;
	}
    }

    return 0;
}

static unsigned int rb_x11_ret(struct rb_x11 *rb_x11)
{
    if (rb_x11->catch_errors) {
	unsigned int err;

	XSync(rb_x11->dpy, false);
	err = rb_x11->last_error;
	rb_x11->last_error = Success;
	return err;
    }

    return Success;
}

static bool rb_x11_setup_formats(struct rb_x11 *rb_x11)
{
    struct rb_x11_format *formats;
    static XRenderPictFormat templ = {.type = PictTypeDirect};
    int i = 0;
    XRenderPictFormat *std_formats[STD_MAX];
    enum rb_std_format j;

    while (XRenderFindFormat(rb_x11->dpy, PictFormatType, &templ, i))
	++i;

    rb_x11->num_formats = i;
    formats = calloc(rb_x11->num_formats, sizeof(*formats));
    if (!formats)
	return false;

    for(j = STD_ARGB32; j < STD_MAX; ++j) {
	std_formats[j] = XRenderFindStandardFormat(rb_x11->dpy,
						   std_format_map[j]);
	if (!std_formats[j])
	    goto out_no_format;
    }

    for (i = 0; i < rb_x11->num_formats; ++i) {
	struct rb_format *rb_form;
	XRenderPictFormat *form;

	form  = XRenderFindFormat(rb_x11->dpy, PictFormatType, &templ, i);
	rb_form = &formats[i].rb_form;
	memcpy(rb_form, &form->direct, sizeof(form->direct));
	rb_form->depth = form->depth;
	formats[i].form = form;
	for (j = STD_ARGB32; j < STD_MAX; ++j) {
	    if (form == std_formats[j])
		rb_x11->std_formats[j] = i;
	}
    }

    rb_x11->formats = formats;

    return true;

  out_no_format:
    free(formats);
    return false;
}

static struct rb_image *rb_x11_get_image(struct rb *rb,
					 struct rb_drawable *draw,
					 int x, int y, int w, int h)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    return (struct rb_image *) XGetImage(rb_x11->dpy, (Drawable) draw,
					 x, y, w, h, 0xffffffff, ZPixmap);
}

static void rb_x11_destroy_image(struct rb_image *image)
{
    XDestroyImage((XImage *) image);
}

static unsigned long rb_x11_get_pixel(const struct rb_image *image, int x, int y)
{
    return XGetPixel((XImage *) image, x, y);
}

static bool rb_x11_next_test(struct rb *rb)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);
    XEvent ev;


    while (XNextEvent(rb_x11->dpy, &ev) == 0) {
	if (ev.type == Expose && !ev.xexpose.count)
	    return true;
    }

    return false;
}

static void rb_x11_destroy(struct rb *rb)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    free(rb_x11->win.name);
    XCloseDisplay(rb_x11->dpy);
    free(rb_x11->formats);
    free(rb_x11);
}

static struct rb_drawable *
rb_x11_create_drawable(struct rb *rb, int width, int height, int depth)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    return (struct rb_drawable *)
        XCreatePixmap(rb_x11->dpy, DefaultRootWindow(rb_x11->dpy),
                      width, height, depth);
}

static void
rb_x11_destroy_drawable(struct rb *rb, struct rb_drawable *rbd)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    XFreePixmap(rb_x11->dpy, (Pixmap) rbd);
}

static void
rb_x11_destroy_picture(struct rb *rb, struct rb_picture *picture)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    XRenderFreePicture(rb_x11->dpy, (Picture) picture);
}

static struct rb_picture *
rb_x11_create_picture(struct rb *rb, struct rb_drawable *draw,
		      const struct rb_format *format,
		      unsigned long valuemask,
		      const XRenderPictureAttributes *attributes)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);
    const struct rb_x11_format *format_x11 =
	(struct rb_x11_format *) format;

    return (struct rb_picture *)
	XRenderCreatePicture(rb_x11->dpy, (Drawable) draw, format_x11->form,
			       valuemask, attributes);
}

static struct rb_picture *
rb_x11_create_solidfill(struct rb *rb, const struct rb_color *c)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    return (struct rb_picture *)
	XRenderCreateSolidFill(rb_x11->dpy, (const XRenderColor *) c);
}

static struct rb_picture *
rb_x11_create_linear_gradient(struct rb *rb, const XLinearGradient *grad,
			      const XFixed *stops, const struct rb_color *colors,
			      int nstops) {
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    return (struct rb_picture *)
	XRenderCreateLinearGradient(rb_x11->dpy, grad, stops,
				    (const XRenderColor *) colors, nstops);
}

unsigned int
rb_x11_change_picture(struct rb *rb,  struct rb_picture *pict,
		      unsigned long valuemask,
		      const XRenderPictureAttributes *attributes)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    XRenderChangePicture(rb_x11->dpy, (Picture) pict, valuemask, attributes);
    return rb_x11_ret(rb_x11);
}

unsigned int
rb_x11_set_picture_transform(struct rb *rb, struct rb_picture *pict,
			    XTransform *t)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    XRenderSetPictureTransform(rb_x11->dpy, (Picture) pict, t);
    return rb_x11_ret(rb_x11);
}

unsigned int
rb_x11_set_picture_filter(struct rb *rb, struct rb_picture *pict,
			  const char *filter, XFixed *params,
			  int num_params)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    XRenderSetPictureFilter(rb_x11->dpy, (Picture) pict, filter,
			    params, num_params);
    return rb_x11_ret(rb_x11);
}

unsigned int
rb_x11_set_picture_clip_rectangles(struct rb *rb, struct rb_picture *pict,
				   int x_origin, int y_origin,
				   const XRectangle *rects,
				   int num_rect)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    XRenderSetPictureClipRectangles(rb_x11->dpy, (Picture) pict,
				    x_origin, y_origin, rects, num_rect);
    return rb_x11_ret(rb_x11);
}

static struct rb_format *
rb_x11_std_format(struct rb *rb, enum rb_std_format std_format)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    return &rb_x11->formats[rb_x11->std_formats[std_format]].rb_form;
}

static struct rb_format *
rb_x11_supported_format(struct rb *rb, int count)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    if (count >= rb_x11->num_formats)
	return NULL;

    return &rb_x11->formats[count].rb_form;
}

static unsigned int
rb_x11_composite_triangles(struct rb *rb, int op, struct rb_picture *src,
			   struct rb_picture *dst,
			   const struct rb_format *mask_format,
			   int x_src, int y_src,
			   const struct _XTriangle *triangles,
			   int ntri)
{
  struct rb_x11 *rb_x11 = to_rb_x11(rb);
  const struct rb_x11_format *format_x11 =
    (struct rb_x11_format *) mask_format;


  XRenderCompositeTriangles(rb_x11->dpy, op, (Picture) src, (Picture) dst,
			    format_x11->form, 0, 0, triangles, ntri);
  return rb_x11_ret(rb_x11);
}

static unsigned int
rb_x11_composite_trifan(struct rb *rb, int op, struct rb_picture *src,
			struct rb_picture *dst,
			const struct rb_format *mask_format,
			int x_src, int y_src,
			const struct _XPointFixed *points,
			int npoint)
{
  struct rb_x11 *rb_x11 = to_rb_x11(rb);
  const struct rb_x11_format *format_x11 =
    (struct rb_x11_format *) mask_format;


  XRenderCompositeTriFan(rb_x11->dpy, op, (Picture) src, (Picture) dst,
			 format_x11->form, 0, 0, points, npoint);
  return rb_x11_ret(rb_x11);
}

static unsigned int
rb_x11_composite_tristrip(struct rb *rb, int op, struct rb_picture *src,
			  struct rb_picture *dst,
			  const struct rb_format *mask_format,
			  int x_src, int y_src,
			  const struct _XPointFixed *points,
			  int npoint)
{
  struct rb_x11 *rb_x11 = to_rb_x11(rb);
  const struct rb_x11_format *format_x11 =
    (struct rb_x11_format *) mask_format;


  XRenderCompositeTriStrip(rb_x11->dpy, op, (Picture) src, (Picture) dst,
			   format_x11->form, 0, 0, points, npoint);
  return rb_x11_ret(rb_x11);
}

static unsigned int
rb_x11_composite(struct rb *rb, int op, struct rb_picture *src,
		 struct rb_picture *mask, struct rb_picture *dst,
		 int src_x, int src_y, int mask_x, int mask_y,
		 int dst_x, int dst_y, unsigned int width,
		 unsigned int height)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    XRenderComposite(rb_x11->dpy, op, (Picture) src, (Picture) mask,
		     (Picture) dst, src_x, src_y, mask_x, mask_y,
		     dst_x, dst_y, width, height);
    return rb_x11_ret(rb_x11);
}

static unsigned int rb_x11_error_check(struct rb *rb, bool on)
{
    struct rb_x11 *rb_x11 = to_rb_x11(rb);

    if (on && !rb_x11->catch_errors) {
	XSync(rb_x11->dpy, false);
	rb_x11->last_error = Success;
	rb_x11->orig_error_handler = XSetErrorHandler(rb_x11_error_handler);
	rb_x11->next = instances;
	rb_x11->prev = NULL;
	instances = rb_x11;
    } else if (!on && rb_x11->catch_errors) {
	XSync(rb_x11->dpy, false);
	XSetErrorHandler(rb_x11->orig_error_handler);
	if (rb_x11->prev)
	    rb_x11->prev->next = rb_x11->next;
	else
	    instances = rb_x11->next;
    }

    rb_x11->catch_errors = on;

    return rb_x11->last_error;
}



struct rb *rb_x11_init(const char *display, bool is_sync)
{
    struct rb_x11 *rb_x11 = calloc(1, sizeof(*rb_x11));
    struct rb *rb;
    struct _picture_info *window;
    XSetWindowAttributes as;
    XWindowAttributes a;
    int maj, min, i;
    char *format;
    XRenderPictFormat *form;

    if (!rb_x11)
	return NULL;

    rb = &rb_x11->rb;

    window = &rb_x11->win;
    rb_x11->dpy = XOpenDisplay(display);
    if (!rb_x11->dpy) {
	fprintf(stderr, "Couldn't open display.\n");
	goto out_no_dpy;
    }

    if (is_sync)
	XSynchronize(rb_x11->dpy, 1);

    if (!XRenderQueryExtension(rb_x11->dpy, &i, &i)) {
	fprintf(stderr, "Render extension missing.\n");
	goto out_no_render;
    }

    XRenderQueryVersion(rb_x11->dpy, &maj, &min);
    if (maj != 0 || min < 1) {
	fprintf(stderr, "Render extension version too low (%d.%d).\n",
		maj, min);
	goto out_no_render;
    }
    printf("XRender extension version %d.%d\n", maj, min);

    if (maj == 0 && min < 2) {
	printf("Server doesn't support conjoint/disjoint ops, disabling.\n");
	num_ops = PictOpSaturate;
    }

    if (!rb_x11_setup_formats(rb_x11))
	goto out_no_formats;

    window->d = (struct rb_drawable *)
	XCreateSimpleWindow(rb_x11->dpy,
			    DefaultRootWindow(rb_x11->dpy), 0, 0,
			    win_width, win_height, 0, 0,
			    WhitePixel(rb_x11->dpy, 0));

    as.override_redirect = True;
    XChangeWindowAttributes(rb_x11->dpy, (Window) window->d,
			    CWOverrideRedirect, &as);

    XGetWindowAttributes(rb_x11->dpy, (Window) window->d, &a);
    window->format = NULL;
    form = XRenderFindVisualFormat(rb_x11->dpy, a.visual);
    for (i = 0; i < rb_x11->num_formats; ++i) {
	if (form == rb_x11->formats[i].form) {
	    window->format = &rb_x11->formats[i].rb_form;
	    break;
	}
    }
    assert(window->format != NULL);
    window->pict = rb_x11_create_picture(rb, window->d, window->format,
					 0, NULL);
    describe_format(&format, NULL, window->format);
    printf("Window format: %s\n", format);
    asprintf(&window->name, "%s window", format);
    free(format);
    XSelectInput(rb_x11->dpy, (Window) window->d, ExposureMask);
    XMapWindow(rb_x11->dpy, (Window) window->d);

    rb->win = window;
    rb->get_image = rb_x11_get_image;
    rb->destroy_image = rb_x11_destroy_image;
    rb->get_pixel = rb_x11_get_pixel;
    rb->next_test = rb_x11_next_test;
    rb->destroy = rb_x11_destroy;
    rb->create_drawable = rb_x11_create_drawable;
    rb->create_picture = rb_x11_create_picture;
    rb->create_solidfill = rb_x11_create_solidfill;
    if (maj > 0 || min >= 10) {
	rb->create_linear_gradient = rb_x11_create_linear_gradient;
    }
    rb->change_picture = rb_x11_change_picture;
    if (maj > 0 || min >= 6) {
	rb->set_picture_transform = rb_x11_set_picture_transform;
	rb->set_picture_filter = rb_x11_set_picture_filter;
	rb->set_picture_clip_rectangles = rb_x11_set_picture_clip_rectangles;
    }
    rb->std_format = rb_x11_std_format;
    rb->supported_format = rb_x11_supported_format;
    rb->destroy_drawable = rb_x11_destroy_drawable;
    rb->destroy_picture = rb_x11_destroy_picture;
    if (maj > 0 || min >= 4) {
	rb->composite_triangles = rb_x11_composite_triangles;
	rb->composite_trifan = rb_x11_composite_trifan;
	rb->composite_tristrip = rb_x11_composite_tristrip;
    }
    rb->composite = rb_x11_composite;
    rb->error_check = rb_x11_error_check;
    rb->xdpy = rb_x11->dpy;

    return rb;

  out_no_formats:
  out_no_render:
    XCloseDisplay(rb_x11->dpy);
  out_no_dpy:
    free(rb);
    return NULL;
}
