/*
 * Copyright © 2006 Eric Anholt
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Eric Anholt not be used in
 * advertising or publicity pertaining to distribution of the software without
 * specific, written prior permission.  Eric Anholt makes no
 * representations about the suitability of this software for any purpose.  It
 * is provided "as is" without express or implied warranty.
 *
 * ERIC ANHOLT DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL ERIC ANHOLT BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "rendercheck.h"

/**
 * Check SetPictureTransform on a source picture causing a crash.
 */
static bool
bug7366_test_set_picture_transform(struct rb *rb)
{
    struct rb_picture *source_pict;
    struct rb_color color;
    XTransform transform;

    memset(&color, 0, sizeof(color));
    source_pict = rb->create_solidfill(rb, &color);

    memset(&transform, 0, sizeof(transform));
    rb->error_check(rb, true);
    rb->set_picture_transform(rb, source_pict, &transform);
    rb->error_check(rb, false);

    rb->destroy_picture(rb, source_pict);

    return true;
}

/**
 * Check setting of AlphaMap to a source picture causing a crash.
 */
static bool
bug7366_test_set_alpha_map(struct rb *rb)
{
    struct rb_picture *source_pict, *pict;
    struct rb_color color;
    struct rb_drawable *pixmap;
    XRenderPictureAttributes pa;

    memset(&color, 0, sizeof(color));
    source_pict = rb->create_solidfill(rb, &color);

    pixmap = rb->create_drawable(rb, 1, 1, 32);
    pict = rb->create_picture(rb, pixmap, rb->std_format(rb, STD_ARGB32), 0,
			      NULL);

    rb->error_check(rb, true);
    pa.alpha_map = (Picture) source_pict;
    rb->change_picture(rb, pict, CPAlphaMap, &pa);
    rb->error_check(rb, false);

    rb->destroy_drawable(rb, pixmap);
    rb->destroy_picture(rb, pict);
    rb->destroy_picture(rb, source_pict);

    return true;
}

/**
 * Check SetPictureClipRectangles on a source potentially causing a crash.
 */
static bool
bug7366_test_set_picture_clip_rectangles(struct rb *rb)
{
    struct rb_picture *source_pict;
    struct rb_color color;
    XRectangle rectangle;

    memset(&color, 0, sizeof(color));
    source_pict = rb->create_solidfill(rb, &color);

    memset(&rectangle, 0, sizeof(rectangle));
    rb->error_check(rb, true);
    rb->set_picture_clip_rectangles(rb, source_pict, 0, 0, &rectangle, 1);
    rb->error_check(rb, false);

    rb->destroy_picture(rb, source_pict);

    return true;
}

/**
 * Check SetPictureFilter on a source potentially causing a crash.
 */
static bool
bug7366_test_set_picture_filter(struct rb *rb)
{
    struct rb_picture *source_pict;
    struct rb_color color;

    memset(&color, 0, sizeof(color));
    source_pict = rb->create_solidfill(rb, &color);

    rb->error_check(rb, true);
    rb->set_picture_filter(rb, source_pict, "bilinear", NULL, 0);
    rb->error_check(rb, false);

    rb->destroy_picture(rb, source_pict);

    return true;
}

enum rendercheck_result
bug7366_test(struct rb *rb)
{
    if (!rb->set_picture_transform)
	bug7366_test_set_picture_transform(rb);
    bug7366_test_set_alpha_map(rb);
    if (rb->set_picture_clip_rectangles)
	bug7366_test_set_picture_clip_rectangles(rb);
    if (rb->set_picture_filter)
	bug7366_test_set_picture_filter(rb);

    /* If the server isn't gone, then we've succeeded. */
    return R_OK;
}
