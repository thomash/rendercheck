/*
 * Copyright © 2004 Eric Anholt
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Eric Anholt not be used in
 * advertising or publicity pertaining to distribution of the software without
 * specific, written prior permission.  Eric Anholt makes no
 * representations about the suitability of this software for any purpose.  It
 * is provided "as is" without express or implied warranty.
 *
 * ERIC ANHOLT DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL ERIC ANHOLT BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include "rendercheck.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <getopt.h>
#include "version.h"

bool is_verbose = false, minimalrendering = false;
int enabled_tests = ~0;		/* Enable all tests by default */

int format_whitelist_len = 0;
char **format_whitelist = NULL;

/* Number of times to repeat operations so that pixmaps will tend to get moved
 * into offscreen memory and allow hardware acceleration.
 */
int pixmap_move_iter = 1;

int win_width = 40;
int win_height = 200;

int
bit_count(int i)
{
	int count;

	count = (i >> 1) & 033333333333;
	count = i - count - ((count >> 1) & 033333333333);
	count = (((count + (count >> 3)) & 030707070707) % 077);
	/* HAKMEM 169 */
	return count;
}

/* This is not complete, but decent enough for now.*/
void
describe_format(char **desc, const char *prefix, struct rb_format *format)
{
	char ad[4] = "", rd[4] = "", gd[4] = "", bd[4] = "";
	int ac, rc, gc, bc;
	int ashift;

	if (!prefix)
	    prefix = "";

	ac = bit_count(format->alphaMask);
	rc = bit_count(format->redMask);
	gc = bit_count(format->greenMask);
	bc = bit_count(format->blueMask);

	if (ac != 0) {
		snprintf(ad, 4, "a%d", ac);
		ashift = format->alphaShift;
	} else if (rc + bc + gc < format->depth) {
		/* There are bits that are not part of A,R,G,B. Mark them with
		 * an x.
		 */
		snprintf(ad, 4, "x%d", format->depth - rc - gc - bc);
		if (format->redShift == 0 || format->blueShift == 0)
			ashift = format->depth;
		else
			ashift = 0;
	} else
		ashift = 0;

	if (rc  != 0)
		snprintf(rd, 4, "r%d", rc);
	if (gc  != 0)
		snprintf(gd, 4, "g%d", gc);
	if (bc  != 0)
		snprintf(bd, 4, "b%d", bc);

	if (ashift > format->redShift) {
		if (format->redShift > format->blueShift)
			asprintf(desc, "%s%s%s%s%s", prefix, ad, rd, gd, bd);
		else
			asprintf(desc, "%s%s%s%s%s", prefix, ad, bd, gd, rd);
	} else {
		if (format->redShift > format->blueShift)
			asprintf(desc, "%s%s%s%s%s", prefix, rd, gd, bd, ad);
		else
			asprintf(desc, "%s%s%s%s%s", prefix, bd, gd, rd, ad);
	}
}

struct {
    int flag;
    const char *name;
} available_tests[] = {
    {TEST_FILL, "fill"},
    {TEST_DSTCOORDS, "dcoords"},
    {TEST_SRCCOORDS, "scoords"},
    {TEST_MASKCOORDS, "mcoords"},
    {TEST_TSRCCOORDS, "tscoords"},
    {TEST_TMASKCOORDS, "tmcoords"},
    {TEST_BLEND, "blend"},
    {TEST_COMPOSITE, "composite"},
    {TEST_CACOMPOSITE, "cacomposite"},
    {TEST_GRADIENTS, "gradients"},
    {TEST_REPEAT, "repeat"},
    {TEST_TRIANGLES, "triangles"},
    {TEST_BUG7366, "bug7366"},
    {0, NULL}
};

static void
print_test_separator(int i)
{
        if (i % 5 == 0) {
            if(i != 0)
                putc('\n', stderr);
            putc('\t', stderr);
        } else {
            fprintf(stderr, ", ");
        }
}

void print_tests(FILE *file, int tests) {
    int i, j;
    
    for (i=0, j=0; available_tests[i].name; i++) {
        if (!(available_tests[i].flag & tests))
            continue;
	print_test_separator(j++);
        fprintf(stderr, "%s", available_tests[i].name);
        j++;
    }
    for_each_test(test) {
	if (!(test->bit & tests))
	    continue;
	print_test_separator(j++);
        fprintf(stderr, "%s", test->arg_name);
        j++;
    }
    if (j)
        fprintf(file, "\n");
}

_X_NORETURN
static void
usage (char *program)
{
    fprintf(stderr, "usage: %s [-d|--display display] [-v|--verbose]\n"
	"\t[-t test1,test2,...] [-o op1,op2,...] [-f format1,format2,...]\n"
	"\t[-b|--backend backend] [--sync] [--minimalrendering] [--version]\n"
	"Available tests:\n", program);
    print_tests(stderr, ~0);
    fprintf(stderr, "Available backends:\n\txrender, xa\n");
    exit(1);
}

int main(int argc, char **argv)
{
	struct rb *rb;
	int i, o, ret = 1;
	static int is_sync = false, print_version = false;
	static int longopt_minimalrendering = 0;
	char *display = NULL;
	char *backend = "xrender";
	char *test_name, *format, *opname, *nextname;

	static struct option longopts[] = {
		{ "display",	required_argument,	NULL,	'd' },
		{ "iterations",	required_argument,	NULL,	'i' },
		{ "formats",	required_argument,	NULL,	'f' },
		{ "tests",	required_argument,	NULL,	't' },
		{ "ops",	required_argument,	NULL,	'o' },
		{ "backend",	required_argument,	NULL,	'b' },
		{ "verbose",	no_argument,		NULL,	'v' },
		{ "sync",	no_argument,		&is_sync, true},
		{ "minimalrendering", no_argument,
		  &longopt_minimalrendering, true},
		{ "version",	no_argument,		&print_version, true },
		{ NULL,		0,			NULL,	0 }
	};

	while ((o = getopt_long(argc, argv, "d:i:f:t:o:b:v", longopts, NULL)) != -1) {
		switch (o) {
		case 'd':
			display = optarg;
			break;
		case 'i':
			pixmap_move_iter = atoi(optarg);
			break;
		case 'o':
			for (i = 0; i < num_ops; i++)
				ops[i].disabled = true;

			nextname = optarg;
			while ((opname = strsep(&nextname, ",")) != NULL) {
				for (i = 0; i < num_ops; i++) {
					if (strcasecmp(ops[i].name, opname) !=
					    0)
						continue;
					ops[i].disabled = false;
					break;
				}
				if (i == num_ops)
					usage(argv[0]);
			}
			break;
		case 'b':
			backend = optarg;
			break;
		case 'f':
			nextname = optarg;
			for (format_whitelist_len = 0;;format_whitelist_len++)
			{
				if ((format = strsep(&nextname, ",")) == NULL)
					break;
			}

			format_whitelist = malloc(sizeof(char *) *
			    format_whitelist_len);
			if (format_whitelist == NULL)
				errx(1, "malloc");

			/* Now the list is separated by \0s, so use strlen to
			 * step between entries.
			 */
			format = optarg;
			for (i = 0; i < format_whitelist_len; i++) {
				format_whitelist[i] = strdup(format);
				format += strlen(format) + 1;
			}

			break;
		case 't':
			nextname = optarg;

			/* disable all tests */
			enabled_tests = 0;

			while ((test_name = strsep(&nextname, ",")) != NULL) {
				int i;
				bool found = false;
				for(i=0; available_tests[i].name; i++) {
					if(strcmp(test_name, available_tests[i].name) == 0) {
						enabled_tests |= available_tests[i].flag;
						found = true;
						break;
					}
				}
				for_each_test(test) {
					if (strcmp(test_name,
						   test->arg_name) == 0) {
						enabled_tests |= test->bit;
						found = true;
						break;
					}
				}
				if (!found)
					usage(argv[0]);
			}

			break;
		case 'v':
			is_verbose = true;
			break;
		case 0:
			break;
		default:
			usage(argv[0]);
			break;
		}
	}

	minimalrendering = longopt_minimalrendering;

	/* Print the version string.  Bail out if --version was requested and
	 * continue otherwise.
	 */
	printf("rendercheck %s\n", VERSION);
	if (print_version)
		return 0;

	if (strcmp(backend, "xrender") == 0) {
		rb = rb_x11_init(display, is_sync);
	} else if (strcmp(backend, "xa") == 0) {
		rb = rb_xa_init(display ? display : "renderD128");
	} else {
		fprintf(stderr, "No backend named \"%s\".\n", backend);
		usage(argv[0]);
	}

	if (!rb) {
		fprintf(stderr, "Failed to open \"%s\" backend using \"%s\" "
			"display.\n",
			backend,
			display ? display : "default");
		usage(argv[0]);
	}

	for (i = 0; i < num_colors; i++) {
		colors[i].r *= colors[i].a;
		colors[i].g *= colors[i].a;
		colors[i].b *= colors[i].a;
	}

	while (rb->next_test(rb)) {
		if (do_tests(rb))
			ret = 0;
		else
			ret = 1;
		break;
	}

	for (i = 0; i < format_whitelist_len; i++)
		free(format_whitelist[i]);
	free(format_whitelist);

	rb->destroy(rb);
	return ret;
}
