/*
 * Copyright © 2016 Broadcom
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <inttypes.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "rendercheck.h"
#include <X11/extensions/XShm.h>

static XShmSegmentInfo *
get_x_shm_info(struct rb *rb, size_t size)
{
	XShmSegmentInfo *shm_info = calloc(1, sizeof(*shm_info));

	shm_info->shmid = shmget(IPC_PRIVATE, size, IPC_CREAT|0777);
	if (shm_info->shmid < 0) {
		free(shm_info);
		return NULL;
	}

	shm_info->shmaddr = shmat(shm_info->shmid, NULL, 0);
	if (shm_info->shmaddr == (void *)-1) {
		free(shm_info);
		return NULL;
	}

	shm_info->readOnly = false;

	rb->error_check(rb, true);
	XShmAttach(rb->xdpy, shm_info);
	if (rb->error_check(rb, false) == BadAccess)
		fprintf(stderr, "failed to attach shared memory\n");

	return shm_info;
}

static void
fill_shm_with_formatted_color(Display *dpy, XShmSegmentInfo *shm_info,
			      struct rb_format *format,
			      int w, int h, color4d *color)
{
	XImage *image;
	struct rb_format *layout = format;
	unsigned long r = layout->redMask * color->r;
	unsigned long g = layout->greenMask * color->g;
	unsigned long b = layout->blueMask * color->b;
	unsigned long a = layout->alphaMask * color->a;
	unsigned long pix;

	r <<= layout->redShift;
	g <<= layout->greenShift;
	b <<= layout->blueShift;
	a <<= layout->alphaShift;

	pix = r | g | b | a;

	image = XShmCreateImage(dpy, NULL, format->depth, ZPixmap,
				shm_info->shmaddr, shm_info, w, h);
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			XPutPixel(image, x, y, pix);
		}
	}
	XDestroyImage(image);
}

static enum rendercheck_result
test_format(struct rb *rb, int op, int w, int h, struct render_format *format)
{
	Display *dpy = rb->xdpy;
	struct rb_format *argb32_format;
	XShmSegmentInfo *shm_info;
	Pixmap src_pix = 0;
	struct rb_drawable *dst_pix;
	bool pass = true;
	size_t size = w * h * 4;
	color4d dst_color = {.25, .25, .25, .25};
	picture_info src, dst;

	shm_info = get_x_shm_info(rb, size);
	if (!shm_info) {
		pass = false;
		goto fail;
	}

	/* Create the SHM pixmap for the source. */
	src_pix = XShmCreatePixmap(dpy, DefaultRootWindow(dpy),
				   shm_info->shmaddr, shm_info, w, h,
				   format->format->depth);

	src.pict = rb->create_picture(rb, (struct rb_drawable *) src_pix, format->format, 0, NULL);
	src.format = format->format;

	/* Make a plain a8r8g8b8 picture for the dst. */
	argb32_format = rb->std_format(rb, STD_ARGB32);
	dst_pix = rb->create_drawable(rb, w, h, argb32_format->depth);
	dst.pict = rb->create_picture(rb, dst_pix, argb32_format, 0, NULL);
	dst.format = rb->std_format(rb, STD_ARGB32);

	for (int i = 0; i < num_colors; i++) {
		color4d src_color = colors[i];
		color4d expected;
		struct rb_image *image;
		struct rb_format acc;

		fill_shm_with_formatted_color(dpy, shm_info, format->format,
					      w, h, &colors[i]);

		argb_fill(rb, &dst, 0, 0, w, h,
			  dst_color.a, dst_color.r,
			  dst_color.g, dst_color.b);

		rb->composite(rb, op,
			      src.pict, 0, dst.pict,
			      0, 0,
			      0, 0,
			      0, 0,
			      w, h);

		image = rb->get_image(rb, (struct rb_drawable *)dst_pix, 0, 0, w, h);

		color_correct(&src, &src_color);

		accuracy(&acc,
			 argb32_format,
			 format->format);

		do_composite(op, &src_color, NULL, &dst_color, &expected, false);

		for (int j = 0; j < w * h; j++) {
			int x = j % w;
			int y = j / h;
			color4d tested;

			get_pixel_from_image(rb, image, &dst, x, y, &tested);

			if (eval_diff(&acc, &expected, &tested) > 3.) {
				char testname[30];

				pass = false;

				snprintf(testname, ARRAY_SIZE(testname),
					 "%s %s SHM blend", ops[op].name,
					format->name);

				print_fail(testname, &expected, &tested, x, y,
					   eval_diff(&acc, &expected, &tested));
				printf("src color: %.2f %.2f %.2f %.2f\n",
				       src_color.r,
				       src_color.g,
				       src_color.b,
				       src_color.a);
				printf("dst color: %.2f %.2f %.2f %.2f\n",
				       dst_color.r,
				       dst_color.g,
				       dst_color.b,
				       dst_color.a);

				break;
			}
		}

		rb->destroy_image(image);
	}

	rb->destroy_picture(rb, src.pict);
	rb->destroy_picture(rb, dst.pict);
	rb->destroy_drawable(rb, dst_pix);
fail:
	if (shm_info) {
		XFreePixmap(dpy, src_pix);
		XShmDetach(dpy, shm_info);
		/* Wait for server to fully detach before removing. */
		XSync(dpy, False);
		shmdt(shm_info->shmaddr);
		shmctl(shm_info->shmid, IPC_RMID, NULL);
		free(shm_info);
	}

	return pass ? R_OK : R_FAIL;
}

static struct rendercheck_test_result
test_shmblend(struct rb *rb)
{
	Display *dpy = rb->xdpy;
	struct rendercheck_test_result result = {};
	int major, minor, i;
	Bool pixmaps_supported;


	if (!dpy ||
	    !XShmQueryExtension(dpy) ||
	    !XShmQueryVersion(dpy, &major, &minor, &pixmaps_supported) ||
	    !pixmaps_supported) {
		printf("SHM blend test: skipped\n");
		record_result(&result, R_SKIPPED);
		return result;
	}

	for (i = 0; i < nformats; i++) {
		struct render_format *format = &formats[i];

		printf("Beginning SHM blend test from %s\n", format->name);

		record_result(&result, test_format(rb, PictOpSrc, 8, 8,
						   format));
		record_result(&result, test_format(rb, PictOpOver, 8, 8,
						   format));
	}

	return result;
}

DECLARE_RENDERCHECK_ARG_TEST(shmblend, "SHM Pixmap blending",
			     test_shmblend);
