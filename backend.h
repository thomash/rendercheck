/**************************************************************************
 *
 * Copyright © 2018 VMware, Inc., Palo Alto, CA., USA
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDERS, AUTHORS AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#ifndef R_BACKEND_H
#define R_BACKEND_H
#include <stdbool.h>
#include <X11/extensions/Xrender.h>

struct rb_image;
struct rb_drawable;
struct rb_picture;

struct rb_color {
    unsigned short red;
    unsigned short green;
    unsigned short blue;
    unsigned short alpha;
};

/*
 * Mask is unshifted, so to get the blue color channel one does
 *
 * color = (pixel >> format.shift) & format.mask;
 */
struct rb_format {
    short redShift;
    short redMask;
    short greenShift;
    short greenMask;
    short blueShift;
    short blueMask;
    short alphaShift;
    short alphaMask;
    short depth;
};

/*
 * Enum values used to get the standard format for a certain depth
 */
enum rb_std_format {
    STD_ARGB32,
    STD_RGB24,
    STD_A8,
    STD_A4,
    STD_A1,
    STD_MAX
};

/*
 * See the XRender documentation for a detailed description of the
 * render functionality.
 *
 * Functions returning an unsigned int typically return Success if OK or
 * BadImplementation if the rendering mode is not implemented and the
 * test should be skipped. If error_check is on, then the backend
 * runs synchronously and reports errors according to the spec rather
 * than aborting on such errors
 */
struct rb {
    /* Optional pointer to X connection. For X specific tests */
    void *xdpy;

    /* Fake or real window */
    struct _picture_info *win;

    /* Get an image from a drawable. An image is a CPU buffer */
    struct rb_image *(*get_image)(struct rb *rb,
				  struct rb_drawable *draw,
				  int x, int y, int w, int h);

    /* Destroy the image */
    void (*destroy_image)(struct rb_image *image);

    /* Get a pixel from an image */
    unsigned long (*get_pixel)(const struct rb_image *image,
			       int x, int y);

    /* Return true if OK to start test run */
    bool (*next_test)(struct rb *rb);

    /* Destroy the backend */
    void (*destroy)(struct rb *rb);

    /* Create a drawable. Storage allocation may be deferred. Mandatory. */
    struct rb_drawable *(*create_drawable)(struct rb *rb, int width, int height,
					   int depth);

    /* Create a picture from a drawable. Mandatory. */
    struct rb_picture *(*create_picture)(struct rb *rb,
					 struct rb_drawable *draw,
					 const struct rb_format *format,
					 unsigned long valuemask,
					 const XRenderPictureAttributes
					 *attributes);

    /* Create a solidfill picture. Mandatory. */
    struct rb_picture *(*create_solidfill)(struct rb *rb,
					   const struct rb_color *c);

    /* Create a linear gradient picture. Optional. */
    struct rb_picture *(*create_linear_gradient)(struct rb *rb,
						 const XLinearGradient *grad,
						 const XFixed *stops,
						 const struct rb_color *colors,
						 int nstops);

    /* Change certain picture attributes. Mandatory. */
    unsigned int (*change_picture) (struct rb *rb, struct rb_picture *pict,
				    unsigned long valuemask,
				    const XRenderPictureAttributes *attributes);

    /* Set a picture transform. Mandatory. */
    unsigned int (*set_picture_transform) (struct rb *rb,
					   struct rb_picture *pict,
					   XTransform *t);

    /* Set picture clip rectangles. Optional. */
    unsigned int (*set_picture_clip_rectangles) (struct rb *rb,
						 struct rb_picture *pict,
						 int x_origin,
						 int y_origin,
						 const XRectangle *rects,
						 int num_rect);

    /* Set picture filter. Optional. */
    unsigned int (*set_picture_filter) (struct rb *rb,
					struct rb_picture *pict,
					const char *filter,
					XFixed *params,
					int num_params);
    /*
     * Destroy a drawable.
     * Drawables are refcounted by pictures, so it's OK to destroy a
     * drawable while there are pictures pointing to it.
     */
    void (*destroy_drawable)(struct rb *rb, struct rb_drawable *draw);

    /* Destroy a picture. */
    void (*destroy_picture)(struct rb *rb, struct rb_picture *picture);

    /*
     * Get a standard format. Returns NULL if no format is available.
     * Format pointers point to static memory and must not be freed.
     */
    struct rb_format *(*std_format)(struct rb *rb, enum rb_std_format);

    /*
     * Get a supported format from an internal array of supported formats.
     * Returns NULL iff (count >= the number of supported formats).
     * Format pointers point to static memory and must not be freed.
     */
    struct rb_format *(*supported_format)(struct rb *rb, int count);

    /* Composite triangles. Optional. */
    unsigned int (*composite_triangles)(struct rb *rb, int op,
					struct rb_picture *src,
					struct rb_picture *dst,
					const struct rb_format *mask_format,
					int x_src, int y_src,
					const struct _XTriangle *triangles,
					int ntri);

    /* Composite trifan. Optional. */
    unsigned int (*composite_trifan)(struct rb *rb, int op, struct rb_picture
				     *src,
				     struct rb_picture *dst,
				     const struct rb_format *mask_format,
				     int x_src, int y_src,
				     const struct _XPointFixed *points,
				     int npoint);

    /* Composite tristrip. Optional. */
    unsigned int (*composite_tristrip)(struct rb *rb, int op,
				       struct rb_picture *src,
				       struct rb_picture *dst,
				       const struct rb_format *mask_format,
				       int x_src, int y_src,
				       const struct _XPointFixed *points,
				       int npoint);

    /* Composite. Mandatory. */
    unsigned int (*composite) (struct rb *rb, int op, struct rb_picture *src,
			       struct rb_picture *mask, struct rb_picture *dst,
			       int src_x, int src_y, int mask_x, int mask_y,
			       int dst_x, int dst_y, unsigned int width,
			       unsigned int height);
    /*
     * Set error check mode.
     * If set to on, clears previous errors and disables asynchronous
     * abort on errors.
     * If set to off, synchronizes, re-enables asynchronous abort on errors
     * and returns the last error occured.
     */
    unsigned int (*error_check) (struct rb *rb, bool on);
};

/* Initialize the xrender backend. */
struct rb *rb_x11_init(const char *display, bool is_sync);
struct rb *rb_xa_init(const char *dev_name);
#endif
