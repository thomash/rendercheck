/*
 * Copyright © 2005 Eric Anholt
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Eric Anholt not be used in
 * advertising or publicity pertaining to distribution of the software without
 * specific, written prior permission.  Eric Anholt makes no
 * representations about the suitability of this software for any purpose.  It
 * is provided "as is" without express or implied warranty.
 *
 * ERIC ANHOLT DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL ERIC ANHOLT BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>

#include "rendercheck.h"

#define TEST_WIDTH 40
#define TEST_HEIGHT 40

static int dot_colors[5][5] = {
	{7, 7, 7, 7, 7},
	{7, 7, 7, 7, 7},
	{7, 0, 7, 7, 7},
	{7, 7, 7, 7, 7},
	{7, 7, 7, 7, 7},
};

static picture_info *create_dot_picture(struct rb *rb)
{
	picture_info *p;
	int i;

	p = malloc(sizeof(picture_info));

	p->d = rb->create_drawable(rb, 5, 5, 32);
	p->format = rb->std_format(rb, STD_ARGB32);
	p->pict = rb->create_picture(rb, p->d, p->format, 0, NULL);
	p->name = (char *)"dot picture";

	for (i = 0; i < 25; i++) {
		int x = i % 5;
		int y = i / 5;
		color4d *c = &colors[dot_colors[y][x]];

		argb_fill(rb, p, x, y, 1, 1, c->a, c->r, c->g, c->b);
	}

	return p;
}

static void destroy_dot_picture(struct rb *rb, picture_info *p)
{
	rb->destroy_picture(rb, p->pict);
	rb->destroy_drawable(rb, p->d);
	free(p);
}

static void init_transform (XTransform *t)
{
	int i, j;

	for (i = 0; i < 3; i++)
		for (j = 0; j < 3; j++)
			t->matrix[i][j] = XDoubleToFixed((i == j) ? 1 : 0);
}

/* Test drawing a 5x5 source image scaled 8x, as either a source or mask.
 */
enum rendercheck_result
trans_coords_test(struct rb *rb, picture_info *white,
    bool test_mask)
{
	picture_info *win = rb->win;
	color4d tested;
	bool failed = false;
	int tested_colors[TEST_HEIGHT][TEST_WIDTH], expected_colors[TEST_HEIGHT][TEST_WIDTH];
	XTransform t;
	picture_info *src;
	struct rb_image *image;
	int x, y;

#if 0
	/* Skip this test when using indexed picture formats because
	 * indexed color comparisons are not implemented in rendercheck
	 * yet.
	 */
	if (win->format->type == PictTypeIndexed) {
		return R_SKIPPED;
	}
#endif
	src = create_dot_picture(rb);
	if (src == NULL) {
		fprintf(stderr, "couldn't allocate picture for test\n");
		return R_FAIL;
	}

	init_transform(&t);
	t.matrix[2][2] = XDoubleToFixed(8);

	CHECK_IMPL(rb->set_picture_transform(rb, src->pict, &t));

	if (!test_mask)
	    CHECK_IMPL(
		rb->composite(rb, PictOpSrc, src->pict, 0,
			      win->pict, 0, 0, 0, 0, 0, 0,
			      TEST_WIDTH, TEST_HEIGHT));
	else {
	    CHECK_IMPL(
		rb->composite(rb, PictOpSrc, white->pict, src->pict,
			      win->pict, 0, 0, 0, 0, 0, 0,
			      TEST_WIDTH, TEST_HEIGHT));
	}

	image = rb->get_image(rb, win->d, 0, 0, TEST_WIDTH, TEST_HEIGHT);

	for (y = 0; y < TEST_HEIGHT; y++) {
		for (x = 0; x < TEST_WIDTH; x++) {
		int src_sample_x, src_sample_y;

		src_sample_x = x / 8;
		src_sample_y = y / 8;
		expected_colors[y][x] = dot_colors[src_sample_y][src_sample_x];

		get_pixel_from_image(rb, image, win, x, y, &tested);

		if (tested.r == 1.0 && tested.g == 1.0 && tested.b == 1.0) {
			tested_colors[y][x] = 0;
		} else if (tested.r == 0.0 && tested.g == 0.0 &&
		    tested.b == 0.0) {
			tested_colors[y][x] = 7;
		} else {
			tested_colors[y][x] = 9;
		}
		if (tested_colors[y][x] != expected_colors[y][x])
			failed = true;
	    }
	}

	if (failed) {
		printf("%s transform coordinates test failed.\n",
		    test_mask ? "mask" : "src");
		printf("expected vs tested:\n");
		for (y = 0; y < TEST_HEIGHT; y++) {
			for (x = 0; x < TEST_WIDTH; x++)
				printf("%d", expected_colors[y][x]);
			printf(" ");
			for (x = 0; x < TEST_WIDTH; x++)
				printf("%d", tested_colors[y][x]);
			printf("\n");
		}
		printf(" vs tested (same)\n");
		for (y = 0; y < TEST_HEIGHT; y++) {
			for (x = 0; x < TEST_WIDTH; x++)
				printf("%d", tested_colors[y][x]);
			printf("\n");
		}
	}
	rb->destroy_image(image);

	init_transform(&t);

	rb->set_picture_transform(rb, src->pict, &t);

	destroy_dot_picture(rb, src);

	return failed ? R_FAIL : R_OK;
}
