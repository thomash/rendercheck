/**************************************************************************
 *
 * Copyright © 2018 VMware, Inc., Palo Alto, CA., USA
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDERS, AUTHORS AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/
#include "rendercheck.h"
#include <xa_tracker.h>
#include <xa_context.h>
#include <xa_composite.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>

struct rb_xa_format {
    struct rb_format rb_format;
    enum xa_formats xa_format;
    enum xa_formats hw_format;
};

struct rb_xa {
    struct rb rb;
    struct xa_tracker *xat;
    struct _picture_info win;
    int fd;
    const struct xa_composite_allocation *ca;
    bool error_check;
    int maj, min, patch;
};

struct rb_xa_image {
    struct rb_xa *rb_xa;
    void *data;
    unsigned int byte_pitch;
    unsigned int cpp;
};

struct rb_xa_drawable {
    struct xa_surface *srf;
    int refcount;
    int width;
    int height;
    int depth;
};

struct rb_xa_picture {
    struct rb_xa_drawable *draw;
    struct xa_picture *picture;
    struct xa_picture *alpha_map;
    union xa_source_pict *spict;
    struct rb_xa_format format;
    int refcount;
};

static const enum xa_composite_op rb_xa_op_map[] = {
    [PictOpClear] = xa_op_clear,
    [PictOpSrc] = xa_op_src,
    [PictOpDst] = xa_op_dst,
    [PictOpOver] = xa_op_over,
    [PictOpOverReverse] = xa_op_over_reverse,
    [PictOpIn] = xa_op_in,
    [PictOpInReverse] = xa_op_in_reverse,
    [PictOpOut] = xa_op_out,
    [PictOpOutReverse] = xa_op_out_reverse,
    [PictOpAtop] = xa_op_atop,
    [PictOpAtopReverse] = xa_op_atop_reverse,
    [PictOpXor] = xa_op_xor,
    [PictOpAdd] = xa_op_add
};

static const enum xa_formats rb_xa_formats[] = {
    xa_format_a8r8g8b8,
    xa_format_x8r8g8b8,
    xa_format_r5g6b5,
    xa_format_x1r5g5b5,
    xa_format_a8,
#if ((XA_TRACKER_VERSION_MAJOR > 2) || \
     (XA_TRACKER_VERSION_MAJOR == 2 && XA_TRACKER_VERSION_MINOR >= 5))
    xa_format_a4r4g4b4,
    xa_format_a2b10g10r10,
    xa_format_x2b10g10r10,
    xa_format_b8g8r8a8,
    xa_format_b8g8r8x8,
#endif
};

static const unsigned int rb_xa_formats_size =
    ARRAY_SIZE(rb_xa_formats);

static unsigned int num_supported_formats;

static const enum xa_formats rb_xa_standard_formats[] = {
    [STD_ARGB32] = xa_format_a8r8g8b8,
    [STD_RGB24] = xa_format_unknown,
    [STD_A8] = xa_format_a8,
    [STD_A4] = xa_format_unknown,
    [STD_A1] = xa_format_unknown
};

static struct rb_xa_format rb_xa_supp_formats[ARRAY_SIZE(rb_xa_formats)];

static struct rb_xa *to_rb_xa(struct rb *rb)
{
    return (struct rb_xa *) rb;
}

static unsigned int rb_xa_err(struct rb_xa *rb_xa, unsigned int err)
{
    if (err == Success || err == BadImplementation || rb_xa->error_check)
	return err;

    fprintf(stderr, "Trapped XA backend error %u\n", err);
    abort();
}

static void
rb_xa_compute_rb_format(struct rb_xa_format *rb_xa_format)
{
    struct rb_format *format = &rb_xa_format->rb_format;
    enum xa_formats form = rb_xa_format->xa_format;

    if (xa_format_type(form) == xa_type_argb ||
	xa_format_type(form) == xa_type_a) {

	format->blueShift = 0;
	format->blueMask = (1 << xa_format_b(form)) - 1;
	format->greenShift = xa_format_b(form);
	format->greenMask = (1 << xa_format_g(form)) - 1;
	format->redShift = format->greenShift + xa_format_g(form);
	format->redMask = (1 << xa_format_r(form)) - 1;
	format->alphaShift = format->redShift + xa_format_r(form);
	format->alphaMask = (1 << xa_format_a(form)) - 1;
	format->depth = format->alphaShift + xa_format_a(form);

    } else if (xa_format_type(form) == xa_type_abgr) {

	format->redShift = 0;
	format->redMask = (1 << xa_format_r(form)) - 1;
	format->greenShift = xa_format_r(form);
	format->greenMask = (1 << xa_format_g(form)) - 1;
	format->blueShift = format->greenShift + xa_format_g(form);
	format->blueMask = (1 << xa_format_b(form)) - 1;
	format->alphaShift = format->blueShift + xa_format_b(form);
	format->alphaMask = (1 << xa_format_a(form)) - 1;
	format->depth = format->alphaShift + xa_format_a(form);

    } else if (xa_format_type(form) == xa_type_bgra) {
	int a_channel_size = xa_format_a(form);

	if (a_channel_size == 0)
	    a_channel_size = xa_format_bpp(form) - xa_format_argb_depth(form);

	format->alphaShift = 0;
	format->alphaMask = (1 << xa_format_a(form)) - 1;
	format->redShift = a_channel_size;
	format->redMask = (1 << xa_format_r(form)) - 1;
	format->greenShift = format->redShift + xa_format_r(form);
	format->greenMask = (1 << xa_format_g(form)) - 1;
	format->blueShift = format->greenShift + xa_format_g(form);
	format->blueMask = (1 << xa_format_b(form)) - 1;
	format->depth = format->blueShift + xa_format_b(form) +
	    xa_format_a(form) - a_channel_size;
    } else
	assert(0);

    assert(format->depth == xa_format_argb_depth(form));
}


static void
rb_xa_setup_formats(struct rb_xa *rb_xa)
{
    int i;
    uint32_t hw_format;
    uint32_t xa_format;

    for (i = 0; i < rb_xa_formats_size; ++i) {
	xa_format = rb_xa_formats[i];
	hw_format = xa_format_unknown;
	if (xa_format_check_supported(rb_xa->xat, xa_format,
				      XA_FLAG_RENDER_TARGET) == XA_ERR_NONE)
	    hw_format = xa_format;
	else if (xa_format_argb_depth(xa_format) != xa_format_bpp(xa_format) &&
		 xa_format_a(xa_format) == 0) {
	    hw_format = xa_format(
		xa_format_bpp(xa_format),
		xa_format_type(xa_format),
		xa_format_bpp(xa_format) - xa_format_argb_depth(xa_format),
		xa_format_r(xa_format),
		xa_format_g(xa_format),
		xa_format_b(xa_format));
	    if (xa_format_check_supported(rb_xa->xat, hw_format,
					  XA_FLAG_RENDER_TARGET) != XA_ERR_NONE)
		hw_format = xa_format_unknown;
	}

	if (hw_format == xa_format_unknown) {
	    struct rb_xa_format format = {.xa_format = xa_format};
	    char *format_name;

	    rb_xa_compute_rb_format(&format);
	    describe_format(&format_name, NULL, &format.rb_format);
	    printf("XA unsupported format: %s\n", format_name);
	    free(format_name);

	    continue;
	}

	rb_xa_supp_formats[num_supported_formats].xa_format = xa_format;
	rb_xa_supp_formats[num_supported_formats].hw_format = hw_format;
	rb_xa_compute_rb_format(&rb_xa_supp_formats[num_supported_formats]);
	num_supported_formats++;
    }
    printf("XA number of supported formats is %d.\n", num_supported_formats);
}

static struct rb_drawable *
rb_xa_create_drawable(struct rb *rb, int width, int height, int depth)
{
    struct rb_xa_drawable *xad = calloc(1, sizeof(*xad));

    if (!xad) {
	rb_xa_err((struct rb_xa *)rb, BadAlloc);
	return NULL;
    }

    xad->refcount = 1;
    xad->width = width;
    xad->height = height;
    xad->depth = depth;

    return (struct rb_drawable *) xad;
};

static unsigned int
rb_xa_change_picture(struct rb *rb,
		     struct rb_picture *pic, unsigned long valuemask,
		     const XRenderPictureAttributes *attributes)
{
    struct rb_xa_picture *xap = (struct rb_xa_picture *) pic;

    if (valuemask & CPRepeat) {
	xap->picture->wrap = (attributes->repeat) ?
	    xa_wrap_repeat : xa_wrap_clamp_to_border;
	valuemask &= ~CPRepeat;
    }
    if (valuemask & CPComponentAlpha) {
	xap->picture->component_alpha = (attributes->component_alpha) ? 1 : 0;
	valuemask &= ~CPComponentAlpha;
    }

    return (valuemask == 0) ? Success : BadImplementation;
}

static struct rb_picture *
rb_xa_create_picture(struct rb *rb, struct rb_drawable *draw,
		     const struct rb_format *format, unsigned long valuemask,
		     const XRenderPictureAttributes *attributes)
{
    struct rb_xa *rb_xa = to_rb_xa(rb);
    struct rb_xa_drawable *xad = (struct rb_xa_drawable *) draw;
    struct rb_xa_picture *xap;
    struct xa_picture *pic;
    struct rb_xa_format *rb_xa_form = (struct rb_xa_format *) format;
    uint32_t xa_format = rb_xa_form->xa_format;
    unsigned int modifyerr;

    assert(xa_format != xa_format_unknown &&
	   xa_format_depth(xa_format) == format->depth);

    assert(!xad || !xad->srf);

    xap = calloc(1, sizeof(*xap));
    if (!xap)
	goto out_err;

    pic = calloc(1, rb_xa->ca->xa_picture_size);
    if (!pic)
	goto out_no_pic;

    xap->picture = pic;
    xap->spict = NULL;
    xap->refcount = 1;
    xap->draw = xad;
    xap->format = *rb_xa_form;
    if (xad)
	xad->refcount++;

    pic->pict_format = xa_format;
    pic->srf = NULL;
    pic->alpha_map = NULL;
    pic->has_transform = 0;
    pic->component_alpha = 0;
    pic->wrap = xa_wrap_clamp_to_border;

    modifyerr = rb_xa_change_picture(rb, (struct rb_picture *) xap, valuemask,
				     attributes);
    modifyerr = rb_xa_err(rb_xa, modifyerr);
    if (modifyerr != Success)
	goto out_modify_err;

    return (struct rb_picture *) xap;

  out_modify_err:
    free(pic);
  out_no_pic:
    free(xap);
  out_err:
    rb_xa_err(rb_xa, BadAlloc);
    return NULL;
}

static struct rb_picture *
rb_xa_create_solidfill(struct rb *rb, const struct rb_color *c)
{
    struct rb_xa *rb_xa = to_rb_xa(rb);
    struct rb_xa_picture *xap;
    struct xa_picture *pic;
    union xa_source_pict *spic;
    bool set_up = false;

    xap = calloc(1, sizeof(*xap));
    if (!xap)
	goto out_err;

    pic = calloc(1, rb_xa->ca->xa_picture_size);
    if (!pic)
	goto out_no_pic;

    spic = calloc(1, rb_xa->ca->xa_source_pict_size);
    if (!spic)
	goto out_no_spic;

#if ((XA_TRACKER_VERSION_MAJOR > 2) || \
     (XA_TRACKER_VERSION_MAJOR == 2 && XA_TRACKER_VERSION_MINOR >= 5))

    if (rb_xa->maj > 2 || (rb_xa->maj == 2 && rb_xa->min >= 5)) {
	spic->solid_fill.type = xa_src_pict_float_solid_fill;
	spic->float_solid_fill.color[0] = ((float) c->red) / 65536.;
	spic->float_solid_fill.color[1] = ((float) c->green) / 65536.;
	spic->float_solid_fill.color[2] = ((float) c->blue) / 65536.;
	spic->float_solid_fill.color[3] = ((float) c->alpha) / 65536.;
	set_up = true;
    }
#endif
    if (!set_up) {
	spic->solid_fill.type = xa_src_pict_solid_fill;
	spic->solid_fill.color = ((c->alpha >> 8) << 24) |
	    ((c->red >> 8) << 16) |
	    ((c->green >> 8) << 8) |
	    ((c->blue >> 8));
    }

    xap->refcount = 1;
    xap->picture = pic;
    xap->spict = spic;
    pic->src_pict = spic;

    pic->pict_format = xa_format_unknown;
    pic->srf = NULL;
    pic->alpha_map = NULL;
    pic->has_transform = 0;
    pic->component_alpha = 0;
    pic->wrap = xa_wrap_repeat;

    return (struct rb_picture *) xap;

  out_no_spic:
    free(pic);
  out_no_pic:
    free(xap);
  out_err:
    rb_xa_err(rb_xa, BadAlloc);

    return NULL;
}

static unsigned int
rb_xa_set_picture_transform(struct rb *rb, struct rb_picture *pict,
			    XTransform *t)
{
    struct rb_xa_picture *xap = (struct rb_xa_picture *) pict;
    struct xa_picture *pic = xap->picture;
    int i;
    int j;

    pic->has_transform = 0;

    if (t) {
	for ( i = 0; i < 3; ++i)
	    for (j = 0; j < 3; ++j)
		pic->transform[i + j*3] = XFixedToDouble(t->matrix[i][j]);

	pic->has_transform = 1;
    }

    return Success;
}

static void
rb_xa_destroy_drawable(struct rb *rb, struct rb_drawable *draw)
{
    struct rb_xa_drawable *xad = (struct rb_xa_drawable *) draw;

    if (--(xad->refcount) > 0)
	return;

    if (xad->refcount < 0)
	abort();

    if (xad->srf)
	xa_surface_unref(xad->srf);

    free(xad);
}

static void
rb_xa_destroy_picture(struct rb *rb, struct rb_picture *pict)
{
    struct rb_xa_picture *xap = (struct rb_xa_picture *) pict;

    if (--(xap->refcount) > 0)
	return;

    if (xap->refcount < 0)
	abort();

    if (xap->draw)
	rb_xa_destroy_drawable(rb, (struct rb_drawable *)xap->draw);

    if (xap->spict)
	free(xap->spict);
    free(xap->picture);
    free(xap);
}

static struct rb_image *
rb_xa_get_image(struct rb *rb, struct rb_drawable *draw,
		int x, int y, int w, int h)
{
    struct rb_xa *rb_xa = to_rb_xa(rb);
    struct rb_xa_drawable *xad = (struct rb_xa_drawable *) draw;
    struct rb_xa_image *xai = calloc(1, sizeof(*xai));
    int ret;
    struct xa_box box = {x, y, x + w, y + h};

    if (!xai)
	goto out_err;

    assert(xad->srf != NULL);
    xai->cpp = xa_format_bpp(xa_surface_format(xad->srf)) / 8;
    xai->byte_pitch = xai->cpp*w;

    xai->data = malloc(xai->byte_pitch * h);
    if (!xai->data)
	goto out_no_data;

    xai->rb_xa = rb_xa;
    ret = xa_surface_dma(xa_context_default(rb_xa->xat),
			 xad->srf, xai->data, xai->byte_pitch, 0,
			 &box, 1);
    if (ret != XA_ERR_NONE)
	goto out_no_dma;

    return (struct rb_image *) xai;

  out_no_dma:
    free(xai->data);
  out_no_data:
    free(xai);
  out_err:
    rb_xa_err(rb_xa, BadAlloc);
    return NULL;
}

static void rb_xa_destroy_image(struct rb_image *image)
{
    struct rb_xa_image *xai = (struct rb_xa_image *) image;

    free(xai->data);
    free(xai);
}

static unsigned long
rb_xa_get_pixel(const struct rb_image *image, int x, int y)
{
    const struct rb_xa_image *xai = (const struct rb_xa_image *) image;
    void *addr = ((uint8_t *) xai->data) + xai->byte_pitch * y +
	xai->cpp * x;

    switch(xai->cpp) {
    case 8:
      return *(uint8_t *) addr;
    case 16:
	return *(uint16_t *)addr;
    default:
	break;
    }

    return *(uint32_t *)addr;
}

static struct rb_format *
rb_xa_std_format(struct rb *rb, enum rb_std_format f)
{
    enum xa_formats form;
    unsigned int i;

    if (f >= STD_MAX)
	return NULL;

    form = rb_xa_standard_formats[f];
    if (form == xa_format_unknown)
	return NULL;

    for(i = 0; i < num_supported_formats; ++i) {
	if (rb_xa_supp_formats[i].xa_format == form)
	    return &rb_xa_supp_formats[i].rb_format;
    }

    return NULL;
}

static struct rb_format *
rb_xa_supported_format(struct rb *rb, int count)
{
    if (count >= num_supported_formats)
	return NULL;

    return &rb_xa_supp_formats[count].rb_format;
}

static void
rb_xa_destroy(struct rb *rb)
{
    struct rb_xa *rb_xa = to_rb_xa(rb);

    free(rb_xa->win.name);
    rb_xa_destroy_drawable(rb, rb_xa->win.d);
    rb_xa_destroy_picture(rb, rb_xa->win.pict);
    xa_tracker_destroy(rb_xa->xat);
    close(rb_xa->fd);
    free(rb_xa);
}

static bool
rb_xa_next_test(struct rb *rb)
{
    return true;
}

static unsigned int
rb_xa_populate_picture(struct rb_xa *rb_xa,
		       struct rb_xa_picture *xap,
		       bool as_dst)
{
    struct rb_xa_drawable *xad;
    uint32_t flags = 0;

    if (xap->spict)
	return rb_xa_err(rb_xa, (as_dst) ? BadDrawable : Success);

    xad = xap->draw;
    if (!xad)
	return rb_xa_err(rb_xa, BadDrawable);

    if (as_dst)
	flags |= XA_FLAG_RENDER_TARGET;

    if (!xad->srf) {
	xad->srf = xa_surface_create(rb_xa->xat, xad->width, xad->height,
				     xad->depth, xa_type_other,
				     xap->format.hw_format, flags);
    } else {
	int ret = xa_surface_redefine(xad->srf, xad->width, xad->height,
				      xad->depth, xa_type_other,
				      xap->format.hw_format, flags, 1);
	if (ret != XA_ERR_NONE) {
	    xa_surface_unref(xad->srf);
	    xad->srf = NULL;
	}
    }

    if (!xad->srf)
	return rb_xa_err(rb_xa, BadAlloc);

    /* Note, no refcounting here. */
    xap->picture->srf = xad->srf;

    return Success;
}


static unsigned int
rb_xa_composite(struct rb *rb, int op, struct rb_picture *src,
		struct rb_picture *mask, struct rb_picture *dst,
		int src_x, int src_y, int mask_x, int mask_y,
		int dst_x, int dst_y, unsigned int width,
		unsigned int height)
{
    struct xa_composite comp;
    struct rb_xa *rb_xa = to_rb_xa(rb);
    struct rb_xa_picture *xsrc = (struct rb_xa_picture *)src;
    struct rb_xa_picture *xmask = (struct rb_xa_picture *)mask;
    struct rb_xa_picture *xdst = (struct rb_xa_picture *)dst;
    unsigned int ret;
    struct xa_context *ctx = xa_context_default(rb_xa->xat);

    if (op >= ARRAY_SIZE(rb_xa_op_map))
	return BadImplementation;

    comp.src = (xsrc) ? xsrc->picture : NULL;
    comp.mask = (xmask) ? xmask->picture : NULL;
    comp.dst = (xdst) ? xdst->picture : NULL;
    comp.op = rb_xa_op_map[op];
    comp.no_solid = false;

    if (xa_composite_check_accelerated(&comp) != XA_ERR_NONE)
	return rb_xa_err(rb_xa, BadImplementation);

    if (xsrc) {
	ret = rb_xa_populate_picture(rb_xa, xsrc, false);
	if (ret != Success)
	    return rb_xa_err(rb_xa, ret);
    }

    if (xmask) {
	ret = rb_xa_populate_picture(rb_xa, xmask, false);
	if (ret != Success)
	    return rb_xa_err(rb_xa, ret);
    }

    if (!xdst)
	return BadDrawable;

    ret = rb_xa_populate_picture(rb_xa, xdst, true);
    if (ret != Success)
	return rb_xa_err(rb_xa, ret);

    if (xa_composite_prepare(ctx, &comp) != XA_ERR_NONE)
	return rb_xa_err(rb_xa, BadImplementation);

    xa_composite_rect(ctx, src_x, src_y, mask_x, mask_y, dst_x, dst_y,
		      width, height);
    xa_composite_done(ctx);

    return Success;
}


struct _picture_info *
rb_xa_create_win(struct rb_xa *rb_xa)
{
    struct _picture_info *win = &rb_xa->win;
    char *format;

    win->format = &rb_xa_supp_formats[1].rb_format;
    win->d = rb_xa_create_drawable(&rb_xa->rb, win_width, win_height,
				   win->format->depth);
    win->pict = rb_xa_create_picture(&rb_xa->rb, win->d,
				     &rb_xa_supp_formats[1].rb_format, 0, NULL);
    describe_format(&format, NULL, win->format);
    printf("Fake XA window format: %s\n", format);
    asprintf(&win->name, "%s fake XA window", format);

    return win;
}

static unsigned int
rb_xa_error_check(struct rb *rb, bool on)
{
    struct rb_xa *rb_xa = to_rb_xa(rb);

    rb_xa->error_check = on;
    return 0;
}

struct rb *rb_xa_init(const char *dev_name)
{
    struct rb_xa *rb_xa = calloc(1, sizeof(*rb_xa));
    struct rb *rb;
    char file_name[128];

    if (!rb_xa)
	return NULL;

    rb = &rb_xa->rb;
    snprintf(file_name, sizeof(file_name), "/dev/dri/%s", dev_name);
    rb_xa->fd = open(file_name, O_RDWR);
    if (rb_xa->fd < 0)
	goto out_no_file;

    rb_xa->xat = xa_tracker_create(rb_xa->fd);
    if (!rb_xa->xat)
	goto out_no_xa;

    xa_tracker_version(&rb_xa->maj, &rb_xa->min, &rb_xa->patch);
    printf("XA version is %d.%d.%d\n", rb_xa->maj, rb_xa->min, rb_xa->patch);

    rb_xa_setup_formats(rb_xa);
    rb_xa->ca = xa_composite_allocation();

    rb->destroy = rb_xa_destroy;
    rb->get_pixel = rb_xa_get_pixel;
    rb->destroy_image = rb_xa_destroy_image;
    rb->get_image = rb_xa_get_image;
    rb->std_format = rb_xa_std_format;
    rb->supported_format = rb_xa_supported_format;
    rb->create_drawable = rb_xa_create_drawable;
    rb->create_picture = rb_xa_create_picture;
    rb->change_picture = rb_xa_change_picture;
    rb->create_solidfill = rb_xa_create_solidfill;
    rb->set_picture_transform = rb_xa_set_picture_transform;
    rb->destroy_drawable = rb_xa_destroy_drawable;
    rb->destroy_picture = rb_xa_destroy_picture;
    rb->composite = rb_xa_composite;
    rb->error_check = rb_xa_error_check;
    rb->next_test = rb_xa_next_test;
    rb->win = rb_xa_create_win(rb_xa);
    num_ops = PictOpSaturate;

    return rb;

 out_no_xa:
    close(rb_xa->fd);
 out_no_file:
    free(rb_xa);

    return NULL;
}
